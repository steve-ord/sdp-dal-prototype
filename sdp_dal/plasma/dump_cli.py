"""
Command line utility for dumping contents of Apache Plasma store

Usage:
  sdp-dump-plasma [-q | -qq] [options] <plasma-socket> [<prefix>]

Options:
  -q                 Show values, don't resolve
  -qq                Show only object types
  --precision=[x]    Precision to use for tensor output [default: 4]
  --threshold=[x]    Maximum number of tensor elements to show [default: 100]
  --edgeitems=[x]    Number of tensor elements to show at edges [default: 2]
  --follow           Follow new objects as they get sealed
  --evict            Periodically evict store (force object deletion)
  --evict-bytes=[x]  Bytes to evict [default: 1000000]
"""

import pyarrow
from pyarrow import plasma
from . import common
from tabulate import tabulate
from si_prefix import si_format
import docopt
import numpy

import binascii
from datetime import datetime
import sys
import select
from typing import Dict
import traceback

def show_data(data, datas, verbosity=2):
    """ Show Apache Arrow IPC stream

    :param data: Raw data to interpret as IPC stream
    :param datas: Dictionary for looking up referenced data
    :param verbosity: 0: column names, 1: columns + values, 2: referenced data
    :return: String representation of data
    """

    # A record batch?
    try:
        return show_record_batch(data, datas, verbosity)
    except OSError as e:
        if not str(e).endswith("but got unknown"):
            traceback.print_exc()
        pass

    # A tensor?
    try:

        # Attempt to interpret as tensor. This is actually a standard
        # IPC stream with a single message under the hood, so the code
        # below would also work up until the point where we attempt to
        # make sense of the payload (where for the purpose of the
        # Python API we are literally stuck?)
        tensor = pyarrow.ipc.read_tensor(pyarrow.BufferReader(data))
        out = f"{tensor.type}[{','.join([ str(s) for s in tensor.shape])}]"
        if verbosity > 0:
            out += "\n" + str(tensor.to_numpy())
        return out
    except OSError as e:
        if not str(e).endswith("Message is not Tensor."):
            traceback.print_exc()
        pass

    try:

        # Attempt to read content
        out = "Generic Arrow message stream"
        if verbosity > 0:
            for msg in pyarrow.ipc.MessageReader.open_stream(pyarrow.BufferReader(data)):
                out += "\n" + str(msg)
        return out

    except Exception as e:
        return 'Not a valid Arrow message stream'

def format_field(fld :pyarrow.Field, sep :str='', verbosity :int=2) -> str:
    """ Format a field / column name for showing to the user.

    Adds type and metadata information as appropriate.
    :param fld: Field
    :param sep: Separator between name and type
    :param verbosity: >=1: show type information
    :returns: String representation
    """
    out = fld.name
    if verbosity > 0:
        out += sep + ":" + str(fld.type)
        if not fld.nullable:
            out += " !null"
        if fld.metadata is not None:
            for name, val in fld.metadata.items():
                out += sep + f"{name.decode()}={val.decode()}"
    return out

def format_field_val(v :pyarrow.ArrayValue,
                     fld :pyarrow.Field,
                     datas :Dict[plasma.ObjectID, pyarrow.Buffer]=None,
                     verbosity :int=2) -> str:
    """ Format a value for showing to the user.

    Mainly takes care of formatting Plasma ObjectIDs appropriately,
    possibly looking up the referenced object
    :param v: Value to show
    :param fld: Value field (for metadata)
    :param datas: Data lookup
    :param verbosity: >=2: look up referenced data
    :returns: String representation
    """

    if isinstance(v, pyarrow.FixedSizeBinaryValue):

        # Format as hexadecimal string
        v = v.as_py()
        out = binascii.hexlify(v).decode('ascii')

        # Reference?
        if verbosity > 1 and datas is not None and \
           len(v) == common.OBJECT_ID_SIZE and \
           fld.metadata is not None and \
           common.PROC_PAR_META in fld.metadata:
            v = plasma.ObjectID(v)
            if v in datas:
                out += f"\n-> {show_data(datas[v], datas, verbosity-2)}"
            else:
                out += "\n-> not found"
        return out
    return str(v)

def show_record_batch(data, datas=None, verbosity=2):
    """ Show record batch data

    :param data: Raw data to interpret as record batch
    :param datas: Dictionary for looking up referenced data
    :param verbosity: 0: column names, 1: columns + values, 2: referenced data
    :raises OSError: Data does not, in fact, encode a tensor
    :return: String representation of data
    """

    if data is None or data.size == 0:
        return "(empty)"
    reader = pyarrow.ipc.open_stream(pyarrow.BufferReader(data))

    # Show meta data
    meta = []
    if reader.schema.metadata is not None:
        for name, val in reader.schema.metadata.items():
            meta.append(name.decode() + "=" + val.decode())

        # Is a namespace?
        header = f"Table {', '.join(meta)}\n"
        if common.PROC_NAMESPACE_META in reader.schema.metadata:
            try:
                return header + show_namespace(reader, verbosity)
            except Exception as e:
                return header + str(e)
    else:
        header = "Table\n"

    # Create table header
    headers = [ format_field(reader.schema.field(i), '\n', verbosity)
                for i in range(len(reader.schema.names)) ]

    if verbosity == 0:
        return f"table {', '.join(meta)} [{', '.join(headers)}]"

    rows = []
    for batch in reader:
        iters = [ iter(col) for col in batch.columns ]
        for i in range(batch.num_rows):
            rows.append([ format_field_val(next(it), reader.schema.field(i), datas, verbosity)
                          for i, it in enumerate(iters) ])

    # Return table
    return header + tabulate(rows, headers=headers, tablefmt='fancy_grid')

def show_namespace(reader, verbosity):
    """ Show namespace data

    :param data: Record batch reader for call schemas
    :param verbosity: 0: column names, 1: columns + values, 2: referenced data
    :return: String representation of data
    """

    # Show supported call schemas
    name_col = reader.schema.get_field_index('name')
    schema_col = reader.schema.get_field_index('schema')
    calls = []
    for batch in reader:
        iters = [ iter(col) for col in batch.columns ]
        for i in range(batch.num_rows):
            name = next(iters[name_col])
            schema_buf = next(iters[schema_col]).as_buffer()
            schema = pyarrow.ipc.open_stream(pyarrow.BufferReader(schema_buf)).schema
            fields = [ format_field(schema.field(i), ' ', verbosity)
                       for i in range(len(schema.names)) ]
            calls.append(f"* {name}({', '.join(fields)})")
    return "\n".join(calls)


def main(argv):
    """ Command line interface implementation."""

    # Parse command line
    args = docopt.docopt(__doc__, options_first=True, argv=argv)
    verbosity = 2-args['-q']

    # Connect to Plasma server
    client = plasma.connect(args['<plasma-socket>'])

    # Evict first, if requested
    if args['--evict']:
        nbytes = client.evict(int(args['--evict-bytes']))
        if nbytes > 0:
            print(f"evicted {nbytes} bytes")

    # Get all objects and the associated data
    objs = client.list()
    datas = dict(zip(objs.keys(), client.get_buffers(objs.keys(), 0)))

    # Set numpy print options
    numpy.set_printoptions(precision=int(args['--precision']),
                           threshold=int(args['--threshold']),
                           edgeitems=int(args['--edgeitems']))

    # Show all objects with prefix
    prefix = args['<prefix>']
    for oid in sorted(objs.keys(), key=lambda oid: oid.binary()):
        if prefix is not None and \
           not binascii.hexlify(oid.binary()).decode('ascii').startswith(prefix):
            continue

        obj = objs[oid]
        data = datas[oid]

        # Show data about the object
        print(f"{common.object_id_hex(oid)} [{obj['state']}, {obj['ref_count']} refs] "+
              f"{datetime.fromtimestamp(obj['create_time'])}:",end='')
        print(f" {si_format(obj['data_size'],precision=2)}B", end='')
        if obj['metadata_size'] > 0:
            print(f", metadata {si_format(obj['metadata_size'],precision=2)}B", end='')

        # Show object itself
        if data is None:
            print('(vanished before it could be queried)')
        else:
            print('', '\n  '.join(show_data(data, datas, verbosity).split('\n')))
        data = None # Make sure we do not continue holding a reference

    # Follow?
    datas = None
    if args['--follow']:
        client.subscribe()
        socket = client.get_notification_socket()
        while True:

            # Wait for notification. Using select for this has the
            # marked advantage that SIGINT actually works.
            try:
                rs, _, _ = select.select([socket.fileno()], [], [], 1)
            except KeyboardInterrupt:
                break
            if len(rs) == 0:
                if args['--evict']:
                    nbytes = client.evict(int(args['--evict-bytes']))
                    if nbytes > 0:
                        print(f"evicted {nbytes} bytes")
                continue
            oid, data_size, metadata_size = client.get_next_notification()

            # Deletion?
            if data_size < 0:
                print(f"deleted {common.object_id_hex(oid)}")
                if oid in objs:
                    del objs[oid]
                continue
            if oid in objs:
                continue
            data = client.get_buffers([oid], 0)[0]

            # Show?
            if prefix is not None and \
               not binascii.hexlify(oid.binary()).decode('ascii').startswith(prefix):
                continue

            # Show data about the object.
            # NOTE: Yes, this is all somewhat duplicated from above -
            # the reason is that we get somewhat less information from
            # the notifications compared to the full object table (no
            # ref count, for instance).
            print(f"new {common.object_id_hex(oid)} [sealed] "+
                  f"{datetime.today()}:",end='')
            print(f" {si_format(data_size,precision=2)}B", end='')
            if metadata_size > 0:
                print(f", metadata {si_format(metadata_size,precision=2)}B", end='')

            # Show object itself
            if data is None:
                print('(vanished before it could be queried)')
            else:
                print('', '\n  '.join(show_data(data, None, verbosity).split('\n')))
            data = None # Make sure we do not continue holding a reference

import pyarrow.plasma as plasma
import numpy as np
from generators import UVWGenerator

if __name__ == "__main__":

    client = plasma.connect("/tmp/plasma")
    id = plasma.ObjectID(np.random.bytes(20))

    uvw_gen = UVWGenerator()
    uvw = uvw_gen.generate(arguments=dict())

    obj_id = client.put(uvw)

    retrieved_uvw = client.get(obj_id)

    print(retrieved_uvw)

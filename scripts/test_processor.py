
import sdp_dal.plasma as rpc

import sys
import pyarrow
from pyarrow import plasma

USE_CREATE_AND_SEAL = False

class TestProcessor(processor.Processor):

    def _process_call(self, proc_name, batch):

        # Read parameters
        val = self.parameter(batch, 'val', pyarrow.int32())
        in_arr = self.tensor_parameter(batch, 'inp', pyarrow.int32(), 1)

        # The actual calculation
        out_arr = in_arr + val

        # Write to output
        self.output_tensor(batch, 'out', out_arr, pyarrow.int32())

if len(sys.argv) < 3:
    print("Please provide plasma connector and ID prefix!")
    print("  Example: python test_processor.py /tmp/plasma 00000000")
    exit(1)

# Create processor, process calls forever
proc = TestProcessor(sys.argv[1], common.parse_hex_objectid(sys.argv[2]))
while True:
    proc.process()

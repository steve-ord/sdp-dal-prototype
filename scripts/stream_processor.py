
import sdp_dal.plasma as rpc
import pyarrow
import numpy

import time
import sys


STREAM_PROCS = [
    rpc.make_call_schema('stream', [
        rpc.make_tensor_input_par('input', pyarrow.float64(), ['x']),
        rpc.make_tensor_output_par('tag', pyarrow.float64(), []),
    ])
]
                         
class StreamCaller(rpc.Caller):
    def __init__(self, *args, **kwargs):
        super(StreamCaller, self).__init__(STREAM_PROCS, *args, **kwargs)

class StreamProcessor(rpc.Processor):

    def __init__(self, *args, **kwargs):
        super(StreamProcessor, self).__init__(STREAM_PROCS, *args, **kwargs)
        self.bytes_received = 0
    
    def _process_call(self, proc_name, batch):

        if proc_name != 'stream':
            raise ValueError("Call to unknown procedure {}!".format(proc_name))

        # Get input
        inp = self.tensor_parameter(batch, 'input', pyarrow.float64())

        # Log number of bytes received
        self.bytes_received += memoryview(inp).nbytes

        # Tag done
        self.output_tensor(batch, 'tag', numpy.empty(0))
        
if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Please provide plasma socket path!")
        print("  Example: python stream_processor.py /tmp/plasma")
        exit(1)

    # Create processor, process calls forever
    report_rate = 1
    proc = StreamProcessor(sys.argv[1])
    checkpoint = time.time() + report_rate
    while True:
        try:
            while time.time() >= checkpoint:
                print(proc.bytes_received / report_rate / 1e9, "GB/s")
                proc.bytes_received = 0
                checkpoint += report_rate
            proc.process(max(0, checkpoint - time.time()))
        except KeyboardInterrupt:
            exit(0)
